# 🧰 Toolbox

A local Toolbox project with discord bot controller

## 💻 Technologies utilisées
<div>
  <img src="https://img.shields.io/badge/Node.js-43853D?style=for-the-badge&logo=node.js&logoColor=white">
  <img src="https://img.shields.io/badge/Electron-2B2E3A?style=for-the-badge&logo=electron&logoColor=white">
  <img src="https://img.shields.io/badge/MySQL-00000F?style=for-the-badge&logo=mysql&logoColor=white">
  <img src="https://img.shields.io/badge/Discord.py-7289DA?style=for-the-badge&logo=discord&logoColor=white">
</div>

## Deployment

-Toolbox Project
  Make sure you have Node.js installed in your computer

  Install :
  npm install electron
	npm install mysql
	npm install electron-packager --save-dev

  Update project and run it localy :
  npm update
	npm start

  Create .exe in build folder :
  npx electron-packager . DevToolLab --platform=win32 --arch=x64 --out=builds --icon=img/logo.ico

-Bot discord Toolbox :

	Install :
	pip install discord.py
	pip install mysql-connector-python

	command bot :
	%add [categorie] [name] [description] [link] [span] [lien]
	%list
	%delete [name]
	%search [name]
	%update [categorie] [name] [description] [link] [span] [lien]
	%categorie [categorie]